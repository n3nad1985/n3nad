// Store modules
const gulp = require( 'gulp' ),
glob = require( 'glob' ),
es = require( 'event-stream' ),
cond = require( 'gulp-cond' ),
rename = require( 'gulp-rename' ),
gutil = require( 'gulp-util' ),
sass = require( 'gulp-sass' ),
autoprefixer = require( 'gulp-autoprefixer' ),
uglify = require( 'gulp-uglify' ),
browserify = require( 'browserify' ),
babelify = require( 'babelify' ),
watchify = require( 'watchify' ),
source = require( 'vinyl-source-stream' ),
buffer = require( 'vinyl-buffer' ),
del = require( 'del' ),
sourcemaps = require( 'gulp-sourcemaps' ),
{ argv } = require( 'yargs' ),
browserSync = require( 'browser-sync' ).create(),
reload = browserSync.reload,
dotenv = require( 'dotenv' );

// load env variables from variables.env file
dotenv.config({ path: './variables.env' });

// file paths
const cssIn         = 'assets/sass/**/*.scss',
      cssOut        = './',
      jsMainEntry   = 'assets/js/src/theme/index.js',
      jsMainIn      = 'assets/js/src/common/*.js',
      jsMainOut     = 'assets/js';

// set evnironment based on whether --prod flag is present or not
if ( argv.prod ) {
    process.env.NODE_ENV = 'production';
}
const PROD = process.env.NODE_ENV === 'production';

/*######################
### HELPER FUNCTIONS ###
######################*/

/**
* Main helper for perform bundling
*
* @param {Array|Boolean} files files that has been change if any
* @return void
*/
function bundle( files ) {
    // if any file has been updated log it to console
    if( files ) {
      files.map(function ( file ) {
          gutil.log( gutil.colors.bgBlue( 'File updated' ), gutil.colors.blue( file ) );
      });
    }

    // run task
    this.b.bundle()
    .on( 'error', function( err ) {
        // log errors
        gutil.log( gutil.colors.bgRed( 'Browserify error:' ), gutil.colors.red( err ) );
    })
    .pipe( source( this.fileName ) )
    .pipe( buffer() )
    .pipe( cond(
        this.stripCatNames,
        rename( { dirname: '' } ) // strip dinamically generated paths
    ))
    .pipe(
        sourcemaps.init( { loadMaps: true } )
    )
    .pipe( cond(
        this.minify,
        uglify()
    ) )
    .pipe(
        sourcemaps.write( './' )
    )
    .pipe(
        gulp.dest( this.fileOut )
    );
}

/**
* Setup context for bundle function and run it
*
* @param {String} fileName desired name of outputed file
* @param {String} fileOut desired path for the outputed file
* @param {Object} browserifyConfig config file for browserify bundler
* @param {Boolean} stripCatNames if function is used for multiple paths it ensures that outputed paths are relative to root
* @param {Boolean} minify should output file be minified
* @return void
*/
function setupAndRunBundle( fileName, fileOut, browserifyConfig, stripCatNames = null, minify = true ) {

    // bundler reference
    let b;

    // define context for bundle helper
    const bundleContext = {
        fileName,
        fileOut,
        stripCatNames,
        minify
    };

    // if we're in production bundle modules with browserify,
    // otherwise wrap it in watchify and watch
    if ( PROD ) {
        b = browserify( browserifyConfig );
        // add bundler to bundle context
        bundleContext.b = b;
    } else {
        // add watchify config
        const watchifyConfig = Object.assign( {}, watchify.args, browserifyConfig );

        // browserify / watchify init
        b = watchify( browserify( watchifyConfig ) );
        // add bundler to bundle context
        bundleContext.b = b;

        // on modules update call bundle and reload browsers
        b.on( 'update', ( files ) => {
            bundle.call( bundleContext, files );
            reload();
        });
    }

    // initial bundler call
    bundle.call( bundleContext );
}

/*###########
### TASKS ###
###########*/

gulp.task( 'clean', function () {
    del.sync( [ 'assets/js/*.js', './*.css', './**/*.map' ] );
} );

gulp.task( 'css', function () {

    // helper for error handling
    const errorHandler = err => {
        // log errors
        gutil.log( err );
        // show errors in browser
        browserSync.notify( err );
    };
    // task
    gulp.src( cssIn )
        .pipe(
            sass( { outputStyle: 'compressed' } )
        )
        .on( 'error', errorHandler )
        .pipe(
            autoprefixer({
                browsers: [ '>= 1%', 'ie > 10' ]
            })
        )
        .on( 'error', errorHandler )
        .pipe(
            gulp.dest( cssOut )
        );

});

gulp.task( 'jsMain', function () {

    // babelify conf
    const babelify = [ 'babelify', {
        presets: [
            [ 'env', {
                'targets': {
                    'browsers': [ '>= 1%', 'ie > 10' ]
                }
            }],
            [ 'react' ]
        ],
        plugins: [ 'transform-runtime' ] }
    ];

    // browserify config object
    const browserifyConfig = {
        entries: jsMainEntry,
        transform: !PROD ? [ babelify ] :
        [
            babelify,
            [ 'uglifyify', { global: true } ]
        ],
        plugin: PROD ? [ 'bundle-collapser/plugin' ] : [] // if we're in production add plugin
    }

    // setup and run bundle process
    setupAndRunBundle( 'theme.js', jsMainOut, browserifyConfig, null, PROD );

});

gulp.task( 'watch', [ 'clean', 'css', 'jsMain' ], function () {

    // if we're in production don't watch
    if ( PROD ) return;

    // give some room for all scripts to load in order for proxy to catch it
    setTimeout( () => {
        // init browser reloading
        browserSync.init({
            files: './**/*.php',
            proxy: process.env.LOCALHOST,
            online: false
        });
    }, 5000);

    // watch css files (rest have been watched by watchify)
    gulp.watch( cssIn, [ 'css', reload ] );
});

gulp.task( 'default', [ 'clean', 'css', 'jsMain', 'watch' ] );
