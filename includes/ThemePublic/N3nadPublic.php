<?php
namespace N3nad\ThemePublic;
/**
 * ThemePublic class setup public hooks
 *
 * @since 1.0.0
 * @package N3nad
 * @subpackage N3nad/public
 */
class N3nadPublic {

    /**
     * @var {String} $version
     * @access private
     */
    private $version;

    /**
     * Class construct setup version
     *
     * @since 1.0.0
     * @access public
     * @param {String} $ver theme version
     */
    function __construct( $ver ) {

        $this->version = $ver;

    }

    /**
     * Enque fronend styles and scripts
     *
     * @since 1.0.0
     * @access public
     */
    function enque_styles_and_scripts() {

        // Load styles
        wp_enqueue_style( 'montserrat-google-font', 'https://fonts.googleapis.com/css?family=Montserrat:400,900' );
        wp_enqueue_style( 'n3nad-style', get_stylesheet_uri() );

        // Load scripts
    	wp_enqueue_script( 'theme', get_template_directory_uri() . '/assets/js/theme.js', array( 'jquery' ), $this->version, true );

    }

    /**
     * Setup hooks for all core setup
     *
     * @since 1.0.0
     * @access public
     */
    function setup_hooks() {

        add_action( 'wp_enqueue_scripts', array( $this, 'enque_styles_and_scripts' ) );

    }

}

 ?>
