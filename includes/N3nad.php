<?php
namespace N3nad;
/**
 * N3nad theme main class
 *
 * @since 1.0.0
 * @package N3nad
 */
class N3nad {

    /**
     * @var {String} $name
     * @access private
     */
    private $name;

    /**
     * @var {String} $version
     * @access private
     */
    private $version;

    /**
     * @var {Object} $core instance of N3nad\Core
     * @access private
     */
    private $core;

    /**
     * @var {Object} $public instance of N3nad\Public
     * @access private
     */
    private $public;

    /**
     * @var {Object} $admin instance of N3nad\Public
     * @access private
     */
    private $admin;

    /**
     * Setup n3nad name and version and load all dependencies
     *
     * @since 1.0.0
     * @access public
     */
    function __construct( $core, $public, $admin ) {

        $this->name = 'n3nad';
        $this->version = '1.0';

        $this->core = $core;
        $this->public = $public;

        $this->admin = $admin;

    }

    /**
     * Setup all theme functionalities
     *
     * @since 1.0.0
     * @access public
     */
    function setup() {
        // init setups
        $this->core->setup_hooks();
        $this->public->setup_hooks();

    }

}


?>
