<?php
namespace N3nad\ThemeCore;
/**
 * N3nadCore class setup theme support and other core stuff
 *
 * @since 1.0.0
 * @package N3nad
 * @subpackage N3nad/core
 */
class N3nadCore {

    /**
     * Call parent constructor
     */
    function __construct() {

    }

    /**
     * Add theme support for default functionalities
     *
     * @since 1.0.0
     * @access public
     */

    function add_theme_support() {
    	/*
    	 * Make theme available for translation.
    	 * Translations can be filed in the /languages/ directory.
    	 * If you're building a theme based on Akademia, use a find and replace
    	 * to change 'akademia' to the name of your theme in all the template files.
    	 */
    	load_theme_textdomain( 'n3nad', get_template_directory() . '/languages' );

    	// Add default posts and comments RSS feed links to head.
    	add_theme_support( 'automatic-feed-links' );

    	/*
    	 * Let WordPress manage the document title.
    	 * By adding theme support, we declare that this theme does not use a
    	 * hard-coded <title> tag in the document head, and expect WordPress to
    	 * provide it for us.
    	 */
    	add_theme_support( 'title-tag' );

    	/*
    	 * Enable support for Post Thumbnails on posts and pages.
    	 *
    	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
    	 */
    	add_theme_support( 'post-thumbnails' );

    	// Registe menus
    	register_nav_menus( array(
    		'main_menu' => esc_html__( 'Main Menu', 'n3nad' ),
    	) );

    	/*
    	 * Switch default core markup for search form, comment form, and comments
    	 * to output valid HTML5.
    	 */
    	add_theme_support( 'html5', array(
    		'search-form',
    		'comment-form',
    		'comment-list',
    		'gallery',
    		'caption',
    	) );

    	// Set up the WordPress core custom background feature.
    	add_theme_support( 'custom-background', apply_filters( 'theme_custom_background_args', array(
    		'default-color' => 'ffffff',
    		'default-image' => '',
    	) ) );

        /**
         * Set proper picture sizes for responsive use accross site
         */
        update_option( 'medium_size_w', 400 );
        update_option( 'medium_size_h', 225 );
        update_option( 'medium_large_size_w', 800 );
        update_option( 'medium_large_size_h', 800 );
        update_option( 'large_size_w', 1440 );
        update_option( 'large_size_h', 810 );

    }

    /**
     * Initialize widgets area
     *
     * @since 1.0.0
     * @access public
     */
     function widgets_init() {

     	register_sidebar( array(
     		'name'          => esc_html__( 'Sidebar', 'n3nad' ),
     		'id'            => 'sidebar-1',
     		'description'   => esc_html__( 'Add widgets here.', 'n3nad' ),
     		'before_widget' => '<section id="%1$s" class="widget %2$s">',
     		'after_widget'  => '</section>',
     		'before_title'  => '<h2 class="widget-title">',
     		'after_title'   => '</h2>',
     	) );

     }

    /**
    * Responsive images support helper method
    *
    * @since 1.0.0
    * @access public
    * @param {String} $image_id id of the image from media library
    * @param {String} $image_size image size from wp image sizes
    * @param {String} $elem_attr attribute to be added on inserted img element
    */
    function show_image( $image_id, $image_size, $elem_attr = null ) {

        echo wp_get_attachment_image( $image_id, $image_size, false, $elem_attr );

    }

    /**
    * Responsive images URL helper method
    *
    * @since 1.0.0
    * @access public
    * @param {String} $image_id id of the image from media library
    * @param {String} $image_size image size from wp image sizes
    */
    function image_url( $image_id, $image_size ) {

        return wp_get_attachment_image_url( $image_id, $image_size );

    }


    /**
     * Setup hooks for all core setup
     *
     * @since 1.0.0
     * @access public
     */
    function setup_hooks() {
        // Actions
        add_action( 'after_setup_theme', array( $this, 'add_theme_support' ) );
        add_action( 'widgets_init', array( $this, 'widgets_init' ) );

        // Filters

    }

}

 ?>
