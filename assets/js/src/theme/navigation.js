/****************************
* Navigation handler object *
****************************/

// import dependencies
import { TimelineLite } from 'gsap';

/**
* Main menu responsive handler
*
* @since 1.1.0 GSAP animations introduced
* @param {Object} jQuery
* @return {Object} init Initialization method
*/
function Navigation( $ ) {

	// Set up properties
	let isOpen = false,
		resetTimeout,
		$menuToggle = $( '.js-menu-toggle' ),
		$hamburgerLines = $( '.js-hamburger-lines' ),
		$menuContainer = $( '.js-menu-container' ),
		$menuItems = $( '.js-menu-item' ),
		$screen = $( 'html, body' ),
		tl = new TimelineLite( { paused: true } );

	// define menu animations
	tl.to( $menuContainer, 0.2, { y: '0%', scale: 1, opacity: 1 } )
	  .staggerTo( $menuItems, 0.2, { opacity: 1 }, 0.05 );

	/**
	* openMenu() opens menu and handles animations
	*
	* @since 1.0.0
	* @access private
	*/
	function openMenu() {

		// start animation
		tl.play();

		// setup state properties
		isOpen = true;
		$hamburgerLines.addClass( 'is-open' );

		// prevent screen from scrolling
		$screen.addClass( 'no-scroll' );

	}

	/**
	* close menu and handle fade out animations
	*
	* @since 1.0.0
	* @access private
	*/
	function closeMenu() {

		// play reversed in reverse
		tl.reverse();

		// setup state properties
		$hamburgerLines.removeClass( 'is-open' );
		isOpen = false;
		$screen.removeClass( 'no-scroll' );

	}

	/**
	* reset the state if user resize window
	*
	* @since 1.0.0
	* @access private
	*/
	function reset() {

		if ( $( window ).width() > 1199 ) {

			// reset animation and state properties
			tl.pause( 0 );
			$menuContainer.removeAttr( 'style' );
			$menuItems.removeAttr( 'style' );
			$menuToggle.removeClass( 'is-open' );
			$hamburgerLines.removeClass( 'is-open' );
			$screen.removeClass( 'no-scroll' );
			isOpen = false;

		}

	}

	/**
	* debounce calls for reset() method
	*
	* @since 1.0.0
	* @access private
	*/
	function onResize() {

		clearTimeout( resetTimeout );

		resetTimeout = setTimeout( reset, 300 );

	}

	/**
	* initialization method, set up event handlers
	*
	* @since 1.0.0
	* @access public
	*/
	function init() {

		// Attach event handler on hamburger button
		$menuToggle.on( 'click', ( e ) => {

		// If menu is open call openMenu() otherwise call closeMenu()
		if ( !isOpen ) {

			openMenu();

		} else {

			closeMenu();

		}

		});

		// Attach event handler for window resize
		$( window ).on( 'resize', ( e ) => {

			onResize();

		});

	}

	return {
		init
	}

}

export default Navigation;
