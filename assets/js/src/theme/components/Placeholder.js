// import dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactTransitionGroup from 'react-addons-transition-group';
import { TimelineLite, Expo } from 'gsap';

// import components
import Comp from './Comp';

class Placeholder extends Component {

    constructor( props ) {
        super( props );
        // get instance of animation library
        this.tl = new TimelineLite();
    }

    componentDidMount() {
        // this.setState({ mount: true});
        console.log( 'did mount' );
    }

    componentWillAppear(  ) {
        this.tl
            .to( this.slash, 1, { opacity: 1 } )
            .to( this.slash, 1.2, { xPercent: '96%', ease: Expo.easeInOut } )
            .to( this.titleText, 1, { xPercent: '-100%', ease: Expo.easeInOut, delay: -0.5 } )
            .to( this.slash, 1, { xPercent: '-15%', ease: Expo.easeInOut }, 'slashBack' )
            .to( this.titleCover, 1, { scaleX: '1', ease: Expo.easeInOut }, 'slashBack' )
            .to( this.soonText, 1, { xPercent: '+=108%', ease: Expo.easeInOut, delay: -0.85 } )
    }

    /**
    * render method
    */
    render() {

        return (
            <div className="placeholder">
                <div className="placeholder__box">
                    <span className="placeholder__slash" ref={ elem => this.slash = elem }>/</span>
                    <div className="placeholder__title">
                        <div className="placeholder__title-text" ref={ elem => this.titleText = elem }>n3nad</div>
                        <div className="placeholder__title-cover" ref={ elem => this.titleCover = elem }></div>
                    </div>
                    <div className="placeholder__soon">
                        <div className="placeholder__soon-text" ref={ elem => this.soonText = elem }>soon</div>

                    </div>
                </div>
            </div>
        );
    }

}

// Placeholder.propTypes = {
//
// };

export default Placeholder;
