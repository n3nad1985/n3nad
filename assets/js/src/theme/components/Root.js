// import dependencies
import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactTransitionGroup from 'react-addons-transition-group';

// import components
import Placeholder from './Placeholder';

class Root extends Component {

    constructor( props ) {
        super( props );

    }

    render() {
        return (
            <Router>
                <Route path="/" render={ () => (
                    <ReactTransitionGroup component="div">
                        <Placeholder />
                    </ReactTransitionGroup>
                )}>

                </Route>
            </Router>
        );
    }

}

// Root.propTypes = {
//     name: PropTypes.string.isRequired
// };

export default Root;
