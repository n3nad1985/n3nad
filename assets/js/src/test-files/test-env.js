/**
* All objects, mocks and other stuff needed during tests
*
* In this file we can put any code that we need in testing environment
*
*/

const jQuery = {};
const localStorage = {};

global.jQuery = jQuery;
global.localStorage = localStorage;
