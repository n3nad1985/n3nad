<?php
// get autoloader
require 'vendor/autoload.php';

// Theme theme main class
use N3nad\N3nad;
use N3nad\ThemeCore\N3nadCore;
use N3nad\ThemePublic\N3nadPublic;
use N3nad\ThemeAdmin\N3nadAdmin;

// Init setup
$n3nad = new N3nad(
	new N3nadCore(),
	new N3nadPublic( '1.0.0' ),
	new N3nadAdmin()
);
$n3nad->setup();
