<?php
/**
 * Theme entry point
 *
 * All requests are redirected to this file via .htaccess file
 * and Wordpress templating system
 * (if no other template is located, all responses are created through index.php)
 *
 * @package  WordPress
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">

        <?php wp_head(); ?>
    </head>
 	<body>
 		<div id="root">
 			Loading...
 		</div>
        <footer>
            <?php wp_footer(); ?>
        </footer>
 	</body>
 </html>
